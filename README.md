# Dokumentace k aplikaci GpioRest

Vývojář: Jan Bednář 

## Dokumentace požadavků

Předmětem projektu je aplikace, vyžívající serverovou část provozovanou na Raspberry PI. Aplikace využívá REST rozhraní poskytované serverovou částí softwaru WebioPi a tím umožňuje vzdálené ovládání a monitoring vstupně-výstupních portů na Raspberry PI pomocí mobilního telefonu se systémem Android. Dokumentace REST rozhraní WebioPi je dostupná na [https://code.google.com/p/webiopi/wiki/RESTAPI](https://code.google.com/p/webiopi/wiki/RESTAPI).

## Dokumentace designu

#### O aplikaci

Vstupní obrazovka, obsahující grafické znázornění fyzického rozmístění portů na Raspberry PI.

![o-aplikaci.png](https://bitbucket.org/repo/Epa66L/images/1695803104-o-aplikaci.png)

#### Funkce GPIO portů

Obrazovka Funkce GPIO portů obsahuje seznam všech portů, které jsou na Raspberry PI přítomné a umožňuje změnu jejich funkce.

![funkce-gpio-portu.png](https://bitbucket.org/repo/Epa66L/images/2085830475-funkce-gpio-portu.png)

#### Stavy

Obrazovka Stavy obsahuje výpis všech portů. V levém sloupci je ID portu a v pravém současná logická hodnota. Na této obrazovce je také rozbalená roleta nastavení.

![stavy.png](https://bitbucket.org/repo/Epa66L/images/673159972-stavy.png)

#### Nastavení

Tato obrazovka obsahuje ovládací prvky pro nastavení přístupu k Raspberry Pi. Po kliknutí na prvek je otevřeno okno umožňující změnu konkrétního údaje.

![nastaveni.png](https://bitbucket.org/repo/Epa66L/images/2736939370-nastaveni.png)

#### Ovládání GPIO portů

Na této obrazovce je výpis všech portů, které mají nastavenou funkci výstupního portu. Po kliknutí je změněna logická hodnota portu.

![ovladani.png](https://bitbucket.org/repo/Epa66L/images/2252979159-ovladani.png)

## Uživatelská dokumentace

### Nastavení

Při prvním spuštění je nutné nastavit IP adresu, port, uživatelské jméno a heslo ke službě WebioPi. To lze provést na záložce Nastavení (viz. Obrazovka 4). Všechny tyto údaje lze najít v dokumentaci WebioPi, nebo v konfiguračním souboru běžícího daemonu na Raspberry PI.

### Stavy

Sledování aktuálních logických hodnot na výstupech, lze provést skrze tuto obrazovku. Na této obrazovce nelze nic měnit, kliknutí na položku v listu není navázáno na žádnou akci. Port s hodnotou zapnuto má logickou hodnotu „1" a hodnota vypnuto, znamená logickou hodnotu „0".

### Ovládání funkce GPIO portu

Před začátkem samotného ovládání portů je nutné nadefinovat funkci každého z portů, které jsou použity. Port může mít jednu ze dvou funkcí a to vstup anebo výstup. Vstupní porty slouží k  připojení vstupních periferií (tlačítko, fotobuňka, spínač vodní hladiny, …). Výstupní porty slouží k  připojení výstupních periferií (dioda, relé, …).

### Ovládání GPIO portů

Změnu logických hodnot na výstupních portech lze provést skrze tuto obrazovku. Kliknutím na položku v listu dojde k logické operaci negace. To znamená, že port s logickou hodnotou „1" se přepne na log. Hodnotu „0" a port s log. hodnotou „0" se přepne na log. „1".

Na každé obrazovce lze použít funkci obnovit, čímž dojde ke znovunačtení aktuálních údajů.