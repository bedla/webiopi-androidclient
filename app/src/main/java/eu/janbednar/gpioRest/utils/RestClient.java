/*Copyright 2014 Bhavit Singh Sengar
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

package eu.janbednar.gpioRest.utils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class RestClient {
	public static final int GET = 0;
	public static final int POST = 1;
	String response = null;
	JSONObject data;
	String ip;
	String port;
	String username;
	String password;
	String command;

	public RestClient(String ip, String command, String port, int getOrPost,
			String username, String password) {

		this.ip = ip;
		this.port = port;
		this.username = username;
		this.password = password;
		this.command = command;

		HttpParams httpParameters = new BasicHttpParams();
		// Set the timeout in milliseconds until a connection is established.
		// The default value is zero, that means the timeout is not used.
		int timeoutConnection = 3000;
		HttpConnectionParams.setConnectionTimeout(httpParameters,
				timeoutConnection);
		// Set the default socket timeout (SO_TIMEOUT)
		// in milliseconds which is the timeout for waiting for data.
		int timeoutSocket = 5000;
		HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
		HttpClient cli = new DefaultHttpClient(httpParameters);
		HttpGet get;
		HttpPost post;

		try {
			HttpResponse resp = null;
			StringBuilder authPair = new StringBuilder().append(username)
					.append(":").append(password);
			String authentication = Base64.encodeBytes(authPair.toString()
					.getBytes());
			if (getOrPost == GET) {
				get = new HttpGet("http://" + ip + ":" + port + "/GPIO/"
						+ command);
				get.setHeader("Authorization", "Basic " + authentication);
				resp = cli.execute(get);
			}
			;
			if (getOrPost == POST) {
				post = new HttpPost("http://" + ip + ":" + port + "/GPIO/"
						+ command);
				post.setHeader("Authorization", "Basic " + authentication);
				resp = cli.execute(post);
			}
			;

			if (resp.getStatusLine().getStatusCode() == 200) {
				// get response string
				InputStream is = resp.getEntity().getContent();
				BufferedReader br = new BufferedReader(
						new InputStreamReader(is));
				StringBuilder sb = new StringBuilder();
				String line;
				while ((line = br.readLine()) != null) {
					sb.append(line);
				}
				is.close();
				response=sb.toString();

			} else {
				Log.d("Http error", Integer.toString(resp.getStatusLine()
						.getStatusCode()));
			}
			;
		} catch (Exception e) {
			Log.d("error", e.getMessage());
		}
	}

	public JSONObject getJsonObject(){
		if(response!=null){
		try {
			return new JSONObject(response);
		} catch (JSONException e) {
			Log.d("Rest client error", e.getMessage());
		}}
		return new JSONObject();
		}

	public String getResponse() {
		return response;
	}
}