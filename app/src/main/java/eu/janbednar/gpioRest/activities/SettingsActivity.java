package eu.janbednar.gpioRest.activities;

import eu.janbednar.gpioRest.R;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.util.Log;

public class SettingsActivity extends PreferenceActivity {

	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		addPreferencesFromResource(R.xml.settings);

	}

	@SuppressWarnings("deprecation")
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);

		Preference edit1 = findPreference("ip");
		EditTextPreference editt1 = (EditTextPreference) edit1;

		Log.d("ip", String.valueOf(editt1.getText().toString()));
	}

}
