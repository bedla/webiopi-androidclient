package eu.janbednar.gpioRest.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import eu.janbednar.gpioRest.R;
import eu.janbednar.gpioRest.fragments.ControlFragment;
import eu.janbednar.gpioRest.fragments.FunctionFragment;
import eu.janbednar.gpioRest.fragments.MainFragment;
import eu.janbednar.gpioRest.fragments.StateFragment;

public class MainActivity extends ActionBarActivity implements
		ActionBar.OnNavigationListener {

	/**
	 * current dropdown position.
	 */
	private static final String STATE_SELECTED_NAVIGATION_ITEM = "selected_navigation_item";
	private int selectedMenuItem = 0;

	Context context;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		if (getIntent().getBooleanExtra("exit", false)) {
			finish();
			return;
		}

		context = getApplicationContext();
		// Set up the action bar to show a dropdown list.
		final ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);

		// Set up the dropdown list navigation in the action bar.
		actionBar.setListNavigationCallbacks(
				// Specify a SpinnerAdapter to populate the dropdown list.
				new ArrayAdapter<String>(actionBar.getThemedContext(),
						android.R.layout.simple_list_item_1,
						android.R.id.text1, new String[] {
								getString(R.string.title_about),
								getString(R.string.title_functionSection),
								getString(R.string.title_controlSection),
								getString(R.string.title_statesSection), }),
				this);
	}

	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		// Restore the previously serialized current dropdown position.
		if (savedInstanceState.containsKey(STATE_SELECTED_NAVIGATION_ITEM)) {
			getSupportActionBar().setSelectedNavigationItem(
					savedInstanceState.getInt(STATE_SELECTED_NAVIGATION_ITEM));
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// Serialize the current dropdown position.
		outState.putInt(STATE_SELECTED_NAVIGATION_ITEM, getSupportActionBar()
				.getSelectedNavigationIndex());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
		Intent intent = new Intent(getApplicationContext(),
					SettingsActivity.class);
			startActivity(intent);
		}
		if (id == R.id.action_refresh) {
			changeFragment(selectedMenuItem);
		}
		if (id == R.id.action_exit) {
			Intent intent = new Intent(this, MainActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intent.putExtra("exit", true);
			startActivity(intent);
			finish();
		}
		return super.onOptionsItemSelected(item);
	}

	public boolean changeFragment(int position) {
		switch (position) {
		case 1:
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.container, FunctionFragment.newInstance())
					.commit();
			break;
		case 2:
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.container, ControlFragment.newInstance())
					.commit();
			break;
		case 3:
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.container, StateFragment.newInstance())
					.commit();
			break;
		default:
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.container, MainFragment.newInstance())
					.commit();

			break;
		}
		return true;
	}

	@Override
	public boolean onNavigationItemSelected(int position, long id) {
		// When the given dropdown item is selected, show its contents in the
		// container view.

		selectedMenuItem = position;
		return changeFragment(position);
	}

}
