package eu.janbednar.gpioRest.model;

public class Port {

	private int id;

	private String function;

	private int value;

	public Port() {

	}

	public Port(int id, String f, int value) {
		super();
		this.id = id;
		this.function = f;
		this.value = value;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

}
