package eu.janbednar.gpioRest.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import eu.janbednar.gpioRest.utils.RestClient;

public class PortsService {

	String ip;
	String port;
	String username;
	String password;
	public static String IN = "in"; 
	public static String OUT = "out"; 
	public static int ON =1;
	public static int OFF =0;
	public PortsService(Context context) {
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(context.getApplicationContext());
		ip = preferences.getString("ip", "192.168.0.102");
		port = preferences.getString("port", "8000");
		username = preferences.getString("username", "webiopi");
		password = preferences.getString("password", "raspberry");
	}

	/*public String setValue(int portId, boolean newValue) {

		return ip;

	}*/

	public String setFunction(int portId, String function) {
		RestClient rc = new RestClient(ip, Integer.toString(portId)
				+ "/function/" + function, port, RestClient.POST, username,
				password);
		return rc.getResponse();
	}

	public String setValue(int portId, int value) {
		RestClient rc = new RestClient(ip, Integer.toString(portId) + "/value/"
				+ value, port, RestClient.POST, username, password);
		return rc.getResponse();
	}

	public List<Port> getPorts() {
		try {

		
			JSONObject json = new RestClient(ip, "*", port, RestClient.GET,
					username, password).getJsonObject();
			JSONArray names = json.names();
			if(names != null){
			ArrayList<Integer> ids = new ArrayList<Integer>();
			for (int i = 0; i < names.length(); i++) {
				ids.add(Integer.parseInt(names.get(i).toString()));
			}
			Collections.sort(ids);
			List<Port> result = new ArrayList<Port>();
			for (int i = 0; i < ids.size(); ++i) {
				int id = Integer.parseInt(ids.get(i).toString());
				JSONObject portObject = json.getJSONObject(ids.get(i)
						.toString());
				String f = portObject.getString("function");
				int val = portObject.getInt("value");
				Port p = new Port(id, f, val);

				result.add(p);
			}
			return result;
			}
		} catch (Exception e) {
			Log.d("error", e.getMessage());
		}
		return new ArrayList<Port>();
	}
}
