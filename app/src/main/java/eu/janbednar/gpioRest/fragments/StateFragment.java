package eu.janbednar.gpioRest.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import eu.janbednar.gpioRest.R;
import eu.janbednar.gpioRest.model.Port;
import eu.janbednar.gpioRest.model.PortsService;

public class StateFragment extends Fragment {
	View rootView;

	public static StateFragment newInstance() {
		StateFragment fragment = new StateFragment();
		return fragment;
	}

	public StateFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.fragment_state, container, false);
		bindStates(getActivity(),
				(ListView) rootView.findViewById(R.id.stateList));

		return rootView;
	}

	public void bindStates(FragmentActivity activity, ListView listView) {
		new BindItemsToListAssyncTask(activity, listView).execute();
	}

	class BindItemsToListAssyncTask extends
			AsyncTask<String, String, ArrayList<HashMap<String, String>>> {
		ListView listView;
		Activity context;
		ProgressDialog progressDialog;

		public BindItemsToListAssyncTask(FragmentActivity activity,
				ListView listView) {
			this.context = activity;
			this.listView = listView;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = new ProgressDialog(getActivity());
			progressDialog.setMessage(getResources()
					.getString(R.string.loading));
			progressDialog.show();
		}

		@Override
		protected ArrayList<HashMap<String, String>> doInBackground(
				String... params) {

			PortsService ps = new PortsService(context);
			List<Port> ports = new ArrayList<Port>(ps.getPorts());
			ArrayList<HashMap<String, String>> mylist = new ArrayList<HashMap<String, String>>();

			for (Port p : ports) {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put("port", Integer.toString(p.getId()));
				String status = null;
				if (p.getValue()==PortsService.ON) {
					status = getResources().getString(R.string.text_on);
				} else {
					status = getResources().getString(R.string.text_off);
				}
				map.put("state", status);
				mylist.add(map);

			}
			return mylist;
		}

		@Override
		protected void onPostExecute(ArrayList<HashMap<String, String>> result) {
			progressDialog.dismiss();
			if (!result.isEmpty()) {

				SimpleAdapter mSchedule = new SimpleAdapter(getActivity(),
						result, R.layout.state_row, new String[] { "port",
								"state" }, new int[] { R.id.STATE_ID_CELL,
								R.id.STATE_CELL });
				listView.setAdapter(mSchedule);
				listView.setAdapter(mSchedule);
			} else {
				new ConnectionErrorDialogFragment().show(getFragmentManager(),
						"connection_error_dialog");
			}

		}

	}
}
