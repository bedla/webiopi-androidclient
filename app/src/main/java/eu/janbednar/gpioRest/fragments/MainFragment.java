package eu.janbednar.gpioRest.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import eu.janbednar.gpioRest.R;

public class MainFragment extends Fragment {
	/**
	 * The fragment argument representing the section number for this fragment.
	 */

	/**
	 * Returns a new instance of this fragment for the given section number.
	 */
	public static MainFragment newInstance() {
		MainFragment fragment = new MainFragment();

		return fragment;
	}

	public MainFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView;

		rootView = inflater.inflate(R.layout.fragment_main, container, false);
		return rootView;
	}
}
