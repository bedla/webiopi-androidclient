package eu.janbednar.gpioRest.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;
import eu.janbednar.gpioRest.R;
import eu.janbednar.gpioRest.model.Port;
import eu.janbednar.gpioRest.model.PortsService;

public class ControlFragment extends Fragment {
	View rootView;
	ListView listView;
	ArrayList<HashMap<String, String>> listHashMap;
	SimpleAdapter listAdapter;

	public static ControlFragment newInstance() {
		ControlFragment fragment = new ControlFragment();
		return fragment;
	}

	public ControlFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		rootView = inflater
				.inflate(R.layout.fragment_control, container, false);
		listView = (ListView) rootView.findViewById(R.id.controlList);
		bindStates(getActivity(), listView);
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				String value = listHashMap.get(position).get("state");
				int newValue = -1;
				if (value.equalsIgnoreCase(getResources().getString(
						R.string.text_on))) {
					newValue = PortsService.OFF;
				}
				if (value.equalsIgnoreCase(getResources().getString(
						R.string.text_off))) {
					newValue = PortsService.ON;
				}
				int port = Integer.parseInt(listHashMap.get(position).get(
						"port"));
                //listView.setOnClickListener((e)-> startActivity());

                new ChangeValueAssyncTask(getActivity(), port, newValue,
						position).execute();
			}
		});

		return rootView;
	}

	public void bindStates(FragmentActivity activity, ListView listView) {
		new BindItemsToListAssyncTask(activity, listView).execute();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	class BindItemsToListAssyncTask extends
			AsyncTask<String, String, ArrayList<HashMap<String, String>>> {
		ListView listView;
		Activity context;
		ProgressDialog progressDialog;

		public BindItemsToListAssyncTask(FragmentActivity activity,
				ListView listView) {
			this.context = activity;
			this.listView = listView;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = new ProgressDialog(getActivity());
			progressDialog.setMessage(getResources()
					.getString(R.string.loading));
			progressDialog.show();
		}

		@Override
		protected ArrayList<HashMap<String, String>> doInBackground(
				String... params) {

			PortsService ps = new PortsService(context);
			List<Port> ports = new ArrayList<Port>(ps.getPorts());
			listHashMap = new ArrayList<HashMap<String, String>>();

			for (Port p : ports) {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put("port", Integer.toString(p.getId()));
				// map.put("from", "6:30 AM");
				String status = null;
				if (p.getValue()==PortsService.ON) {
					status = getResources().getString(R.string.text_on);
				} else {
					status = getResources().getString(R.string.text_off);
				}
				map.put("state", status);
				if (p.getFunction().equalsIgnoreCase(PortsService.OUT)) {
					listHashMap.add(map);
				}

			}
			return listHashMap;
		}

		@Override
		protected void onPostExecute(ArrayList<HashMap<String, String>> result) {
			progressDialog.dismiss();
			if (!result.isEmpty()) {

				listAdapter = new SimpleAdapter(getActivity(), result,
						R.layout.control_row, new String[] { "port", "state" },
						new int[] { R.id.CONTROL_ID_CELL, R.id.CONTROL_CELL });
				listView.setAdapter(listAdapter);
				listView.setAdapter(listAdapter);
			} else {
				new ConnectionErrorDialogFragment().show(getFragmentManager(),
						"connection_error_dialog");
			}
		}

	}

	class ChangeValueAssyncTask extends AsyncTask<String, String, String> {
		int port;
		int value;
		int positionInList;
		Activity context;
		ProgressDialog progressDialog;

		public ChangeValueAssyncTask(FragmentActivity activity, int port,
				int value, int positionInList) {
			this.context = activity;
			this.value = value;
			this.port = port;
			this.positionInList = positionInList;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = new ProgressDialog(getActivity());
			progressDialog.setMessage(getResources()
					.getString(R.string.loading));
			progressDialog.show();
		}

		@Override
		protected String doInBackground(String... params) {

			PortsService ps = new PortsService(context);
			String response = ps.setValue(port, value);
			if (response.equalsIgnoreCase("0")) {
				listHashMap.get(positionInList).put("state",
						getResources().getString(R.string.text_off));
			}
			;
			if (response.equalsIgnoreCase("1")) {
				listHashMap.get(positionInList).put("state",
						getResources().getString(R.string.text_on));
			}
			;

			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			progressDialog.dismiss();
			if (result != null) {
				listAdapter.notifyDataSetChanged();
				
				StringBuilder sb = new StringBuilder(getResources().getString(R.string.value_of_port));
				sb.append(" ");
				sb.append(port);
				sb.append(" ");
				sb.append(getResources().getString(R.string.is_now));
				sb.append(" ");
				if(result.equalsIgnoreCase("1")){sb.append(getResources().getString(R.string.text_on));};
				if(result.equalsIgnoreCase("0")){sb.append(getResources().getString(R.string.text_off));};
				
				Toast.makeText(getActivity(), sb.toString(),
						Toast.LENGTH_LONG).show();
			} else {
				new ConnectionErrorDialogFragment().show(getFragmentManager(),
						"connection_error_dialog");
			}
		}

	}
}
