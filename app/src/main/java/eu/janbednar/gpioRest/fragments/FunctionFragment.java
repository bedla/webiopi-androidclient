package eu.janbednar.gpioRest.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;
import eu.janbednar.gpioRest.R;
import eu.janbednar.gpioRest.model.Port;
import eu.janbednar.gpioRest.model.PortsService;

public class FunctionFragment extends Fragment {
	View rootView;
	SimpleAdapter listAdapter;
	ArrayList<HashMap<String, String>> listHashMap;
	ListView listView;

	public static FunctionFragment newInstance() {
		FunctionFragment fragment = new FunctionFragment();
		return fragment;
	}

	public FunctionFragment() {
	}

	public void bindFunctions(FragmentActivity activity, ListView listView) {
		new ListFunctionsAssyncTask(activity, listView).execute();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.fragment_function, container,
				false);
		listView = (ListView) rootView.findViewById(R.id.functionList);
		bindFunctions(getActivity(), listView);
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				String function = listHashMap.get(position).get("function");
				String newFunction = null;
				if (function.equalsIgnoreCase(getResources().getString(
						R.string.in))) {
					newFunction = PortsService.OUT;
				}
				if (function.equalsIgnoreCase(getResources().getString(
						R.string.out))) {
					newFunction = PortsService.IN;
				}
				int port = Integer.parseInt(listHashMap.get(position).get(
						"port"));
				if (!function.equalsIgnoreCase(getResources().getString(
						R.string.alt))) {
				new ChangeFunctionAssyncTask(getActivity(), port, newFunction, position)
						.execute();
				}else{
					Toast.makeText(getActivity(), getResources().getString(R.string.this_item_cannot_be_changed),
							Toast.LENGTH_LONG).show();
				}
				
			}
		});

		return rootView;
	}

	class ListFunctionsAssyncTask extends
			AsyncTask<String, String, ArrayList<HashMap<String, String>>> {
		ListView listView;
		Activity context;
		ProgressDialog progressDialog;

		public ListFunctionsAssyncTask(FragmentActivity activity,
				ListView listView) {
			this.context = activity;
			this.listView = listView;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = new ProgressDialog(getActivity());
			progressDialog.setMessage(getResources()
					.getString(R.string.loading));
			progressDialog.show();
		}

		@Override
		protected ArrayList<HashMap<String, String>> doInBackground(
				String... params) {
			PortsService ps = new PortsService(context);
			List<Port> ports = new ArrayList<Port>(ps.getPorts());
			listHashMap = new ArrayList<HashMap<String, String>>();

			for (Port p : ports) {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put("port", Integer.toString(p.getId()));
				String function = null;
				if (p.getFunction().equalsIgnoreCase(PortsService.IN)) {
					function = getResources().getString(R.string.in);
				} else {
					if (p.getFunction().equalsIgnoreCase(PortsService.OUT)) {
						function = getResources().getString(R.string.out);
					} else {
						function = getResources().getString(R.string.alt);
					}
					;
				}
				map.put("function", function);
				listHashMap.add(map);

			}
			return listHashMap;
		}

		@Override
		protected void onPostExecute(ArrayList<HashMap<String, String>> result) {
			progressDialog.dismiss();
			if (result.size()>0) {

				listAdapter = new SimpleAdapter(getActivity(), result,
						R.layout.function_row, new String[] { "port",
								"function" }, new int[] {
								R.id.FUNCTION_ID_CELL, R.id.FUNCTION_CELL });
				listView.setAdapter(listAdapter);
				listView.setAdapter(listAdapter);

			} else {
				new ConnectionErrorDialogFragment().show(getFragmentManager(),
						"connection_error_dialog");
			}
		}

	}

	class ChangeFunctionAssyncTask extends AsyncTask<String, String, String> {
		int port;
		String function;
		Activity context;
		ProgressDialog progressDialog;
		int positionInList;

		public ChangeFunctionAssyncTask(FragmentActivity activity, int port,
				String function, int positionInList) {
			this.context = activity;
			this.function = function;
			this.port = port;
			this.positionInList = positionInList;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = new ProgressDialog(getActivity());
			progressDialog.setMessage(getResources()
					.getString(R.string.loading));
			progressDialog.show();
		}

		@Override
		protected String doInBackground(String... params) {

			PortsService ps = new PortsService(context);
			String response = ps.setFunction(port, function);
			if (response.equalsIgnoreCase(PortsService.IN)) {
				listHashMap.get(positionInList).put("function",
						getResources().getString(R.string.in));
			}
			;
			if (response.equalsIgnoreCase(PortsService.OUT)) {
				listHashMap.get(positionInList).put("function",
						getResources().getString(R.string.out));
			}
			;
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			progressDialog.dismiss();
			if (!result.isEmpty()) {
				listAdapter.notifyDataSetChanged();
				
				StringBuilder sb = new StringBuilder(getResources().getString(R.string.function_of_port));
				sb.append(" ");
				sb.append(port);
				sb.append(" ");
				sb.append(getResources().getString(R.string.is_now));
				sb.append(" ");
				if(result.equalsIgnoreCase(PortsService.IN)){sb.append(getResources().getString(R.string.in));};
				if(result.equalsIgnoreCase(PortsService.OUT)){sb.append(getResources().getString(R.string.out));};

				
				Toast.makeText(getActivity(), sb.toString(),
						Toast.LENGTH_LONG).show();
			} else {
				new ConnectionErrorDialogFragment().show(getFragmentManager(),
						"connection_error_dialog");
			}
		}

	}

}
